public class Eggplant extends Food
{
	public Eggplant()
	{
		id = 3;
		name = "Eggplant";
		price = 4.25f;
	}

	public String getDescription()
	{
		return "farm fresh eggplant pan-fried and served over a bed of rice";
	}
}
